FROM alpine
MAINTAINER faaiqamaruadb
RUN apk update && apk add bash nginx php php-fpm
COPY ./index.php /var/www/html/
COPY ./default.conf /etc/nginx/http.d/
EXPOSE 80
ENTRYPOINT [ "/bin/bash", "-c", "php-fpm82 && nginx -g 'daemon off;'" ]
